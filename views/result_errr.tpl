%import shlex, unicodedata
%def strip_accents(s):
%    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
<div class="search-result">
    %number = (query['page'] - 1)*config['perpage'] + i + 1
    <div class="search-result-number"><a href="#r{{d['sha']}}">#{{number}}</a></div>
    %url = d['url'].replace('file://', '')
    %for dr, prefix in config['mounts'].items():
        %url = url.replace(dr, prefix)
    %end
    <div class="search-result-title" id="r{{d['sha']}}" title="{{d['abstract']}}"><a href="{{url}}">
    %if not d['titulo']:
        {{d['label']}}
    %else:
        {{d['titulo']}}
    %end
    </a></div>
    %if len(d['ipath']) > 0:
        <div class="search-result-ipath">[{{d['ipath']}}]</div>
    %end
    %if  len(d['autores']) > 0:
        <div class="search-result-author">{{d['autores']}}</div>
    %end
    %if  len(d['disponivel_em']) > 0:
        <div style="font-size: 16px;color: #000;font-weight: bold;padding-right: 15px;border-right: 1px solid #333;padding-top: 7px;margin-left: 15px;">
            <a href="{{d['disponivel_em']}}">Link Original</a>
        </div>
    %end
    <div class="search-result-url">
        %urllabel = d['url'].replace('/'+d['filename'],'').replace('file://','')
        %for r in config['dirs']:
            %urllabel = urllabel.replace(r.rsplit('/',1)[0] + '/' , '')
        %end
        <a href="{{url.replace('/'+d['filename'],'')}}">{{urllabel}}</a>
    </div>
    <div class="search-result-date">
    %if d['ano_publicacao'] != "NaN/NaN/NaN" and len(d['ano_publicacao']) > 0:
        {{d['ano_publicacao']}}
    %end
    </div>
    %for q in shlex.split(query['query'].replace("'","\\'")):
        %if not q == "OR":
            % w = strip_accents(q.decode('utf-8').lower()).encode('utf-8')
            % d['snippet'] = d['snippet'].replace(w,'<span class="search-result-highlight">'+w+'</span>')
        %end
    %end
    <div class="search-result-snippet">{{!d['snippet']}}</div>
</div>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
