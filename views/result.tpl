%import shlex, unicodedata, os
<div class="search-result">
    <div class="search-result-title" id="r{{d['sha']}}" title="{{d['abstract']}}">
        <a href="{{d['url_formatada']}}" style="color: #14487A;">
            <h3 style="font-size: 20px; margin-bottom: 10px; margin-top: 30px;">
                %if not d['titulo']:
                    {{d['label']}}
                %else:
                    {{d['titulo']}}
                %end
            </h3>
        </a>
    </div>
    <hr/>
    %if len(d['autores']) > 0:
        <div class="search-result-author" style="color:#000;">Autor(es): {{d['autores']}}</div>
    %end
    %if len(d['tipo_publi']) > 0:
        <div class="" style="color: #000; margin-top:10px;">
            Tipo de publicação: {{d['tipo_publi']}}
        </div>
    %end
    <div class="search-result-publication-type" style="color: #000; margin-top:10px;">
        Publicado em: {{d['tipo_pub_onde_foi_publicado']}}, {{d['volume_numero']}}, {{d['ano_publicacao']}} | {{d['pais']}} | {{d['lingua_pub']}}
    </div>
    %if len(d['disponivel_em']) > 0:
        <div style="font-size: 16px; color: #000; margin-top: 10px;">
            Link para publicação: <a href="{{d['disponivel_em']}}">Link Original</a>
        </div>
    %end
    %if len(d['resp_inserir']) > 0:
        <div style="margin-top: 10px;">
            Metadados inseridos por: {{d['resp_inserir']}}
        </div>
    %end
    %if len(d['indicado_por']) > 0:
        <div style="margin-top: 10px;">
            Indicado por: {{d['indicado_por']}}
        </div>
    %end
    
    <div class="search-result-date">
    %if d['ano_publicacao'] is not None:
        {{d['ano_publicacao']}}
    %else:
        {{d['time']}}
    %end

    </div>
    <hr/>
    <h3 style="display: block; margin-top: 10px; font-size: 20px;">Informações complementares dos autores:</h3>
        <ul>
            <li>['autor01']: ['instituicao_atuacao01'], ['instituicao_atuacao_depto01'] | ['area_formacao01'] | [nacionalidade01]</li>
        </ul>

    <hr/>
    <h3 style="display: block; font-size: 20px;">Resumo</h4>
    <div class="search-result" style="margin-top: 10px;">{{d['resumo']}}</div>
    <h3 style="display: block; font-size: 20px; margin-top: 10px;">Snippet</h4>
    <div class="search-result-snippet" style="margin-top: 10px; font-weight: normal;">{{!d['snippet']}}</div>
</div>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
